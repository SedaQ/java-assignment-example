package org.gopas.java.assignment;

import org.gopas.java.assignment.data.Player;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("What is your given name? Please add");
        String givenName = scanner.next();
        System.out.println("What is your family name? Please add");
        String familyName = scanner.next();

        Player player = new Player();
        player.setFamilyName(familyName);
        player.setGivenName(givenName);

        System.out.println(player);
    }
}
